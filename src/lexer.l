%{
    #include <iostream>
    #include <cstdlib>

    #include "Driver.hpp"
    #include "Lexer.hpp"

    #include "Parser.hpp"
    #include "location.hh"

    #define yyterminate() cn::Parser::make_END(loc);

    #define YY_USER_ACTION loc.columns(yyleng);

    #define YY_NO_UNISTD_H
%}

%option nodefault
%option noyywrap
%option nounput noinput

%option stack

%option debug

%option c++
%option yyclass="Lexer"

%%

%{
    // Handy shortcuts for driver variables.
    // Defined under the %% to be included in the lexer class, not the preamble.
    cn::FileContext *ctx = driver._fileContext;
    cn::location& loc = ctx->_location;
    loc.step();
%}


[ \t]+          {
                    std::string text(yytext);
                    size_t len = text.length();

                    // All indentation should be consistent relative to the first indent in the file.
                    if (ctx->indentWidth == 0)
                    {
                        ctx->indentWidth = len;
                    }
                    else if (len % ctx->indentWidth != 0)
                    {
                        throw cn::Parser::syntax_error(loc,
                            "inconsistent indentation width. " +
                            text + " is not divisible by " + std::to_string(ctx->indentWidth));
                    }

                    // Return the more useful relative change in indentation.
                    size_t indentationLevel = len / ctx->indentWidth;

                    return cn::Parser::make_INDENT(indentationLevel, loc);
                }

\r\n|\n|\r      {
                    // Increase lineno in location and step.
                    loc.lines(1); loc.step();
                    return cn::Parser::make_NL(loc);
                }

#IMPORTLIB.*|#importlib.* {
                    return cn::Parser::make_IMPORTLIB_MACRO(loc);
                }

#.+             {
                    return cn::Parser::make_MACRO(yytext, loc);
                }

class\ [^\n\r]+ {
                    // Hacky replacement for the handleClasses hack.
                    // It'll have to be this way until we have start conditions ironed out.
                    return cn::Parser::make_CLASS(yytext, loc);
                }

struct\ [^\n\r]+ {
                    //ctx->handleStructKeyword();
                    return cn::Parser::make_STRUCT(yytext, loc);
                }

[^ \t\n\r][^\n\r]*  {
                    return cn::Parser::make_EXPR(yytext, loc);
                }

<<EOF>>         {
                    // Close any open brackets in both streams.
                    ctx->writeScopeChanges(ctx->outCpp, ctx->scopeFloor, ctx->indentPrev);
                    ctx->writeScopeChanges(ctx->outHpp, 0, ctx->scopeFloor);
                    ctx->outHpp << "\n#endif";

                    return yyterminate();
                }

%%
