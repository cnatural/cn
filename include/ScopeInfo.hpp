#ifndef SCOPEINFO_H
#define SCOPEINFO_H
#include <string>

class ScopeInfo
{
protected:
    std::string _prefix;
public:
    ScopeInfo(std::string prefix = "") : _prefix(prefix) {};

    std::string prefix()
    {
        return _prefix;
    }

    virtual char const* close() const
    {
        return "}";
    }
};

class ScopeInfoSemiclose : public ScopeInfo
{
public:
    ScopeInfoSemiclose(std::string prefix = "") : ScopeInfo(prefix) {};

    // Feels pretty weird to add the overhead of a virtual call...to return a const char*.
    virtual char const* close() const
    {
        return "};";
    }
};
#endif
