# cn, the cnatural compiler

The cnatural language as implemented here is a lightweight twist on C++: all
expressions in cnatural are valid C++ expressions, and no additional syntax is
added. Instead, some features are replaced by information that will already be
present in most C++ code:
- Indentation level, rather than curly brackets, starts and ends scope blocks
- A newline, rather than a semicolon, ends a statement
- Declarations not in function scope will act as if defined in a hpp file of
the same name as the current file

The `cn` compiler compiles cnatural code to equivalent C++, which can then be
compiled to executables using a standard C++ compiler such as g++. cn is written in C and uses
flex & bison to generate the parser. `cn` is a 'lite' parser - most of the
stream passes straight into a cpp or hpp file without the syntax being
analysed, and in general cn only considers syntax at the start and end of
expressions. The exception to this is situations where a declaration would need
to be represented differently in the header and cpp file.

cnatural and cn are based on Charles Fox's implementation by the same name,
available [here](https://gitlab.com/charles.fox/cnatural).

## Installing cn

### Dependencies
- flex
- bison

e.g. on most apt-based systems:
```
apt-get install flex bison

```
Note: on Ubuntu 18 and earlier, you need to download the tar.gz for bison 3.3 or later. 

### Build
```
Run configure.sh from cn folder
```

The compiler is then available at:
```
/usr/local/bin/cn
Run using cn --help
```

## Demos

Some demonstration programs can be found under `demo/`.

## Wiki

A collection of useful information about cnatural and and known issues can be
found [here](https://gitlab.com/cnatural/cn/-/wikis/home).

Please let us know what you think by [posting an issue on GitLab](https://gitlab.com/cnatural/cn/-/issues).
