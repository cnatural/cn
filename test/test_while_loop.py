from utils import compile_cn, compare_cpp_files, compare_hpp_files, run_cpp
import pytest

@pytest.mark.parametrize('compile_cn', ['WhileLoop'], indirect=['compile_cn'])
def test_cpp(compile_cn):
    compare_cpp_files('WhileLoop.cpp')


@pytest.mark.parametrize('compile_cn', ['WhileLoop'], indirect=['compile_cn'])
def test_hpp(compile_cn):
    compare_hpp_files('WhileLoop.hpp')


@pytest.mark.parametrize('compile_cn', ['WhileLoop'], indirect=['compile_cn'])
def test_code(compile_cn):
    run_cpp('WhileLoop.cpp')