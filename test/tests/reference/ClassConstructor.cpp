#include "ClassConstructor.hpp"

Account::Account(unsigned pounds, unsigned pence)
{
	this->pounds = pounds;
	this->pence = pence;
}

void Account::printBalance()
{
	std::cout << "Balance: " << pounds << "." << pence;
}

int main()
{
	Account m(10, 50);
	m.printBalance();
}
