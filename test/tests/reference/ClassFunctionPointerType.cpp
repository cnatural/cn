#include "ClassFunctionPointerType.hpp"

int *A::a()
{
	return 0;
}

int main()
{
	int *p = (A()).a();
	return 0;
};
