#include "ClassFunction.hpp"

void Account::printBalance(unsigned pounds, unsigned pence)
{
	this->pounds = pounds;
	this->pence = pence;

	std::cout << "Balance: " << pounds << "." << pence;
}

int main()
{
	Account m;
	m.printBalance(10, 50);
}
