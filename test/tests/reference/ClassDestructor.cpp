#include "ClassDestructor.hpp"

Account::Account(unsigned pounds, unsigned pence)
{
	this->pounds = pounds;
	this->pence = pence;
}

Account::~Account()
{
	std::cout << "Account Destroyed";
}

void Account::printBalance()
{
	std::cout << "Balance: " << pounds << "." << pence;
}

int main()
{
	Account m(10, 50);
	m.printBalance();
}
