# Lambda Demo

A simple implementation of the lambda calculus in CNatural.

## Build & Run

```sh
make && ./lambda
```
